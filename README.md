# Statistical Inference Working Directory

This is a "Working Directory" for Statistical Inference Course of Coursera. It consists of several R code examples used in the classes and useful tips, also you can find the assignment related functions and data sets.

## Student
| [Omer Kara](<mailto:okara@ncsu.edu>) |
| :---: |
| Ph.D. Candidate |
| Department of Economics |
| North Carolina State University |

## Notes
- Please do not use any of the content without the permission of the repo owner.
- All comments, suggestions, and other correspondences should be sent to [Omer Kara](<mailto:okara@ncsu.edu>).