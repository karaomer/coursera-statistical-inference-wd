########################### Statistical Inference ##############################
################################ Study Sheet ###################################
################################# Omer Kara ####################################

# Base folder path for the course.
base.path <- "/Volumes/Omer/Dropbox/PhD/Classes/Coursera/Data Science Specialization/6 - Statistical Inference/Statistical Inference_WD"

# Setting the working directory.
setwd(base.path)

################################## Week #1 #####################################
# How to generate random variables
rbinom(n = 10, size = 5, prob = 0.5) ## Binomial. Size is the number of trials for each observation. prob is the probability for success. n is the observation (experiment number)
rnorm(n = 100, mean = 0, sd = 1) ## Normal
rgamma(n = 100, shape = 2, rate = 1) ## Gamma
rpois(n = 100, lambda = 2) ## Poisson
runif(n = 100, min = 0, max = 1) ## Uniform


# Calculating the density, cumulative density and quantiles from given distributions.
dbinom(9, 10, 0.5) ## Getting 9 successes in 10 trials with probability 0.5. Note that d functions return the height of the probability density function.
dnorm(1, mean = 0, sd = 1) ## Probability of a certain value.
pnorm(3, mean = 0, sd = 1) ## Cumulative probability up to the specified value.
qnorm(0.50, 0, 1) ## Returns the x value for given quantile and distribution.
qnorm(pnorm(3, mean = 0, sd = 1), mean = 0, sd = 1) ## Note that pnorm and qnorm functions ar actually inverse of each other. Here we used value 3 to calculate cumulative probability and plug the result into the quantile function and find value 3 again.
pt(3, 30) ## For student t distribution.

# Drawing a density
x <- c(-0.5, 0, 1, 1, 1.5) ## Give the x values.
y <- c( 0, 0, 2, 0, 0) ## Give the y values.
plot(x, y, lwd = 3, frame = FALSE, type = "l") ## "lwd" is the width of the main line.

polygon(c(0, .75, .75, 0), c(0, 0, 1.5, 0), lwd = 3, col = "lightblue") ## Drawing a polygon inside of a density.

pbeta(0.75, 2, 1) ## Calculating the cumulative probability for 0.75 value for beta distribution with parameters 2 and 1. Note that "p" infront of beta function gives you the propability of that density with specified arguments.
pbeta(c(0.4, 0.5, 0.6), 2, 1) ## Does the samething above for different percentages.

qbeta(0.5, 2, 1) ## 50% of the population is less than 0.7071.


# Find the center of the mass
Load.Install(c("UsingR", "ggplot2", "reshape2"))
data(galton)
longGalton <- melt(galton, measure.vars = c("child", "parent"))
g <- ggplot(longGalton, aes(x = value)) + geom_histogram(aes(y = ..density..,  fill = variable), binwidth = 1, colour = "black") + geom_density(size = 0.5)
g <- g + facet_grid(. ~ variable)
g

Load.Install("manipulate") ## Using manipulate package and user written function to see how mean and mean of squared errors change.
myHist <- function(mu){
    g <- ggplot(galton, aes(x = child))
    g <- g + geom_histogram(fill = "salmon", binwidth = 1, aes(y = ..density..), colour = "black")
    g <- g + geom_density(size = 2)
    g <- g + geom_vline(xintercept = mu, size = 2)
    mse <- round(mean((galton$child - mu) ^ 2), 3)
    g <- g + labs(title = paste('mu = ', mu, ' MSE = ', mse))
    g
}
manipulate(myHist(mu), mu = slider(62, 74, step = 0.5))

g <- ggplot(galton, aes(x = child)) ## Doing the same thing but with the sample mean.
g <- g + geom_histogram(fill = "salmon",
                        binwidth = 1, aes(y = ..density..), colour = "black")
g <- g + geom_density(size = 2)
g <- g + geom_vline(xintercept = mean(galton$child), size = 2)
g

# Some plot examples.
ggplot(data.frame(x = factor(0:1), y = c(.5, .5)), aes(x = x, y = y)) + geom_bar(stat = "identity", colour = 'black', fill = "lightblue")

ggplot(data.frame(x = factor(1:6), y = rep(1/6, 6)), aes(x = x, y = y)) + geom_bar(stat = "identity", colour = 'black', fill = "lightblue")

# Drawing a uniform density.
g <- ggplot(data.frame(x = c(-0.25, 0, 0, 1, 1, 1.25),
                       y = c(0, 0, 1, 1, 0, 0)),
            aes(x = x, y = y))
g <- g + geom_line(size = 2, colour = "black")
g <- g + labs(title = "Uniform density")
g


# Simulating a standard normal. It basically creates standard normal with 10 observations and take the mean of these, Then does the same process over and over again. Then plot the distribution of these means.
n <- 10
nosim <- 10000
dat <- data.frame(x = c(rnorm(nosim), apply(matrix(rnorm(nosim * n), nosim), 1, mean)), what = factor(rep(c("Obs", "Mean"), c(nosim, nosim))))
ggplot(dat, aes(x = x, fill = what)) + geom_density(size = 2, alpha = .5);


## Or the other way of doing the simulation is the below loop. Here n is the observation number for each simulation, and nosim is the number of simulatins. This is a better code if you just want to see the distribution of average. But if you want to see the distribution of observations and the averages then use the above code.
n <- 10
nosim <- 10000
data <- data.frame(x = rep(NA, nosim), stringsAsFactors = FALSE)
for (i in 1:nosim) {
    obs <- rnorm(n)
    data$x[i] <- mean(obs)
}
ggplot(data, aes(x = x, fill = "Mean")) + geom_density(size = 2, alpha = .5)


# Averages of x die rolls.
nosim <- 10000
dat <- data.frame(x = c(sample(1:6, nosim, replace = TRUE), apply(matrix(sample(1:6, nosim * 2, replace = TRUE), nosim), 1, mean), apply(matrix(sample(1:6, nosim * 3, replace = TRUE), nosim), 1, mean), apply(matrix(sample(1:6, nosim * 4, replace = TRUE), nosim), 1, mean)), size = factor(rep(1:4, rep(nosim, 4))))
g <- ggplot(dat, aes(x = x, fill = size)) + geom_histogram(alpha = .20, binwidth = .25, colour = "black")
g + facet_grid(. ~ size)


# Averages of x coin flips
nosim <- 10000
dat <- data.frame(x = c(sample(0:1, nosim, replace = TRUE), apply(matrix(sample(0:1, nosim * 10, replace = TRUE), nosim), 1, mean), apply(matrix(sample(0:1, nosim * 20, replace = TRUE), nosim), 1, mean), apply(matrix(sample(0:1, nosim * 30, replace = TRUE), nosim), 1, mean)), size = factor(rep(c(1, 10, 20, 30), rep(nosim, 4))))
g <- ggplot(dat, aes(x = x, fill = size)) + geom_histogram(alpha = .20, binwidth = 1 / 12, colour = "black")
g + facet_grid(. ~ size)


# Assignment #1:
Load.Install(c("swirl", "crayon"))
install_from_swirl("Statistical Inference")
swirl()

integrate(mypdf, 0, 1.6) ## Integrate function with function name, lower and upper bounds.


################################## Week #2 #####################################
# Distribution with increasing variance
xvals <- seq(-10, 10, by = .01)
dat <- data.frame(y = c(dnorm(xvals, mean = 0, sd = 1), dnorm(xvals, mean = 0, sd = 2), dnorm(xvals, mean = 0, sd = 3), dnorm(xvals, mean = 0, sd = 4)), x = rep(xvals, 4), factor = factor(rep(1:4, rep(length(xvals), 4))))
ggplot(dat, aes(x = x, y = y, color = factor)) + geom_line(size = 2)


# Simulating from a population with variance 1. Take 10 standard normals and take variance. Do it ove and over again, then do the same thing with 20 and 30 std normals. Here the sample size is increasing which shows that if you have enough sample the variance of sample will converge to true population variance.
nosim <- 10000
dat <- data.frame(x = c(apply(matrix(rnorm(nosim * 10), nosim), 1, var), apply(matrix(rnorm(nosim * 20), nosim), 1, var), apply(matrix(rnorm(nosim * 30), nosim), 1, var)), n = factor(rep(c("10", "20", "30"), c(nosim, nosim, nosim))))
ggplot(dat, aes(x = x, fill = n)) + geom_density(size = 2, alpha = .2) + geom_vline(xintercept = 1, size = 2)

## Or other way of doing the simulation is taking the 1000 standard normals every time and calculate their variance. Do it 10000 times and plot their distribution.
n <- 1000
nosim <- 10000
data <- data.frame(x = rep(NA, nosim), stringsAsFactors = FALSE)
for (i in 1:nosim) {
    obs <- rnorm(n)
    data$x[i] <- var(obs)
}
ggplot(data, aes(x = x, fill = "Mean")) + geom_density(size = 2, alpha = .5)


# Variances of x die rolls.
nosim <- 10000
dat <- data.frame(x = c(apply(matrix(sample(1:6, nosim * 10, replace = TRUE), nosim), 1, var), apply(matrix(sample(1:6, nosim * 20, replace = TRUE), nosim), 1, var), apply(matrix(sample(1:6, nosim * 30, replace = TRUE), nosim), 1, var)), size = factor(rep(c(10, 20, 30), rep(nosim, 3))))
g <- ggplot(dat, aes(x = x, fill = size)) + geom_histogram(alpha = .20, binwidth = .3, colour = "black")
g <- g + geom_vline(xintercept = 2.92, size = 2)
g + facet_grid(. ~ size)


# Standard normals have variance 1; means of n standard normals have standard deviation 1/sqrt(n)
nosim <- 1000
n <- 10
sd(apply(matrix(rnorm(nosim * n), nosim), 1, mean))
1 / sqrt(n)


# Standard uniforms have variance 1/12, means of random samples of n uniforms have sd 1/sqrt(12*n}
nosim <- 1000
n <- 10
sd(apply(matrix(runif(nosim * n), nosim), 1, mean))
1 / sqrt(12 * n)


# Fair coin flips have variance 0.25, means of random samples of n coin flips have sd 1/(2*sqrt(n) (according to theory)
nosim <- 1000
n <- 10
sd(apply(matrix(sample(0:1, nosim * n, replace = TRUE), nosim), 1, mean))
1 / (2 * sqrt(n))


# Data example
library(UsingR)
data(father.son)
x <- father.son$sheight
n <- length(x)

g <- ggplot(data = father.son, aes(x = sheight)) ## Plot of the son's heights
g <- g + geom_histogram(aes(y = ..density..), fill = "lightblue", binwidth = 1, colour = "black")
g <- g + geom_density(size = 2, colour = "black")
g

round(c(var(x), var(x) / n, sd(x), sd(x) / sqrt(n)), 2) ## Let's interpret these numbers
g


# Binomial distribution
choose(8, 7) * 0.5 ^ 8 + choose(8, 8) * 0.5 ^ 8 ## Choose is for permuation
pbinom(6, size = 8, prob = .5, lower.tail = FALSE)


# The standard normal distribution with reference lines.
x <- seq(-3, 3, length = 1000)
g <- ggplot(data.frame(x = x, y = dnorm(x)), aes(x = x, y = y)) + geom_line(size = 2)
g <- g + geom_vline(xintercept = -3:3, size = 2)
g


# Normal distribution.
qnorm(0.975, mean = 0, sd = 1) ## Gives the Z value corresponding the selected probability.
pnorm(1.96, mean = 0, sd = 1, lower.tail = FALSE) ## Give the upper tail probability for selected z value.
pnorm(1160, mean = 1020, sd = 50, lower.tail = FALSE)
qnorm(0.75, mean = 1020, sd = 50)


# Poisson distribution.
ppois(3, lambda = 2.5 * 4) ## Lambda is 2.5. Question is asking observing 3 or less in 4 hours.
pbinom(2, size = 500, prob = .01) ## Approximates to poisson distribution.
ppois(2, lambda = 500 * .01)


# Law of large numbers in action
n <- 10000
means <- cumsum(rnorm(n)) / (1:n)
g <- ggplot(data.frame(x = 1:n, y = means), aes(x = x, y = y))
g <- g + geom_hline(yintercept = 0) + geom_line(size = 0.9)
g <- g + labs(x = "Number of obs", y = "Cumulative mean")
g

means <- cumsum(sample(0:1, n , replace = TRUE)) / (1:n)
g <- ggplot(data.frame(x = 1:n, y = means), aes(x = x, y = y))
g <- g + geom_hline(yintercept = 0.5) + geom_line(size = 0.9)
g <- g + labs(x = "Number of obs", y = "Cumulative mean")
g


# Central Limit Therom experiment: Die rolling.
nosim <- 10000
cfunc <- function(x, n) sqrt(n) * (mean(x) - 3.5) / 1.71 ## Population mean of die rolling is 3.5 and variance is 1.71.
dat <- data.frame(x = c(apply(matrix(sample(1:6, nosim * 10, replace = TRUE), nosim), 1, cfunc, 10), apply(matrix(sample(1:6, nosim * 20, replace = TRUE), nosim), 1, cfunc, 20), apply(matrix(sample(1:6, nosim * 30, replace = TRUE), nosim), 1, cfunc, 30)), size = factor(rep(c(10, 20, 30), rep(nosim, 3))))
g <- ggplot(dat, aes(x = x, fill = size)) + geom_histogram(alpha = .50, binwidth = .3, colour = "black", aes(y = ..density..))
g <- g + stat_function(fun = dnorm, size = 0.9)
g + facet_grid(. ~ size)


# Central Limit Therom experiment: Coin flipping a fair coin.
nosim <- 1000
cfunc <- function(x, n) 2 * sqrt(n) * (mean(x) - 0.5)
dat <- data.frame(x = c(apply(matrix(sample(0:1, nosim * 10, replace = TRUE), nosim), 1, cfunc, 10), apply(matrix(sample(0:1, nosim * 20, replace = TRUE), nosim), 1, cfunc, 20), apply(matrix(sample(0:1, nosim * 30, replace = TRUE), nosim), 1, cfunc, 30)), size = factor(rep(c(10, 20, 30), rep(nosim, 3))))
g <- ggplot(dat, aes(x = x, fill = size)) + geom_histogram(binwidth = .3, colour = "black", aes(y = ..density..))
g <- g + stat_function(fun = dnorm, size = 0.9)
g + facet_grid(. ~ size)


# Central Limit Therom experiment: Coin flipping a unfair coin where p=0.9. Since the coin is not fair the speed of the approximation need more and more data. Eventually, it will converge.
nosim <- 1000
cfunc <- function(x, n) sqrt(n) * (mean(x) - 0.9) / sqrt(.1 * .9)
dat <- data.frame(x = c(apply(matrix(sample(0:1, prob = c(.1,.9), nosim * 10, replace = TRUE), nosim), 1, cfunc, 10), apply(matrix(sample(0:1, prob = c(.1,.9), nosim * 20, replace = TRUE), nosim), 1, cfunc, 20), apply(matrix(sample(0:1, prob = c(.1,.9), nosim * 30, replace = TRUE), nosim), 1, cfunc, 30)), size = factor(rep(c(10, 20, 30), rep(nosim, 3))))
g <- ggplot(dat, aes(x = x, fill = size)) + geom_histogram(binwidth = .3, colour = "black", aes(y = ..density..))
g <- g + stat_function(fun = dnorm, size = 0.9)
g + facet_grid(. ~ size)


# Give a confidence interval for the average height of sons
library(UsingR)
data(father.son)
x <- father.son$sheight
(mean(x) + c(-1, 1) * qnorm(.975) * sd(x) / sqrt(length(x))) / 12 ## Divided by 12 to have unit in feet.


# Binomial interval
.56 + c(-1, 1) * qnorm(.975) * sqrt(.56 * .44 / 100)
binom.test(56, 100)$conf.int


# Confidence interval simulation with coin flips.
nosim <- 1000
n <- 20
pvals <- seq(.1, .9, by = .05)
coverage <- sapply(pvals, function(p){
    phats <- rbinom(nosim, prob = p, size = n) / n
    ll <- phats - qnorm(.975) * sqrt(phats * (1 - phats) / n)
    ul <- phats + qnorm(.975) * sqrt(phats * (1 - phats) / n)
    mean(ll < p & ul > p)
})
ggplot(data.frame(pvals, coverage), aes(x = pvals, y = coverage)) + geom_line(size = 2) + geom_hline(yintercept = 0.95) + ylim(.75, 1.0)

# Confidence interval simulation with coin flips but adding 2 successes and failures: Agresti-Coull Interval adjusment. Note that interval tend to be conservative.
n <- 20
nosim <- 1000
pvals <- seq(.1, .9, by = .05)
coverage <- sapply(pvals, function(p){
phats <- (rbinom(nosim, prob = p, size = n) + 2) / (n + 4) ## Adjusment is here.
ll <- phats - qnorm(.975) * sqrt(phats * (1 - phats) / n)
ul <- phats + qnorm(.975) * sqrt(phats * (1 - phats) / n)
mean(ll < p & ul > p)
})
ggplot(data.frame(pvals, coverage), aes(x = pvals, y = coverage)) + geom_line(size = 2) + geom_hline(yintercept = 0.95) + ylim(.75, 1.0)


# Confidence interval with poisson distribution.
x <- 5
t <- 94.32
lambda <- x / t
round(lambda + c(-1, 1) * qnorm(.975) * sqrt(lambda / t), 3)
poisson.test(x, T = 94.32)$conf

lambdavals <- seq(0.005, 0.10, by = .01)
nosim <- 1000
t <- 100
coverage <- sapply(lambdavals, function(lambda){
    lhats <- rpois(nosim, lambda = lambda * t) / t
    ll <- lhats - qnorm(.975) * sqrt(lhats / t)
    ul <- lhats + qnorm(.975) * sqrt(lhats / t)
    mean(ll < lambda & ul > lambda)
})
ggplot(data.frame(lambdavals, coverage), aes(x = lambdavals, y = coverage)) + geom_line(size = 2) + geom_hline(yintercept = 0.95) + ylim(0, 1.0)


# Assignment #2:
Load.Install(c("swirl", "crayon"))
install_from_swirl("Statistical Inference")
swirl()


################################## Week #3 #####################################
# Comparison of t and normal distribution.
k <- 1000
xvals <- seq(-5, 5, length = k)
myplot <- function(df){
    d <- data.frame(y = c(dnorm(xvals), dt(xvals, df)), x = xvals, dist = factor(rep(c("Normal", "T"), c(k,k))))
    g <- ggplot(d, aes(x = x, y = y))
    g <- g + geom_line(size = 2, aes(colour = dist))
    g
}
Load.Install("manipulate")
manipulate(myplot(df), df = slider(1, 30, step = 1))

# Comparison of t and normal distribution on quantiles.
pvals <- seq(.5, .99, by = .01)
myplot2 <- function(df){
    d <- data.frame(n = qnorm(pvals), t = qt(pvals, df), p = pvals)
    g <- ggplot(d, aes(x = n, y = t))
    g <- g + geom_abline(size = 2, col = "lightblue")
    g <- g + geom_line(size = 2, col = "black")
    g <- g + geom_vline(xintercept = qnorm(0.975))
    g <- g + geom_hline(yintercept = qt(0.975, df))
    g
}
manipulate(myplot2(df), df = slider(1, 20, step = 1))


# T distribution data example: Paired t test.
data(sleep)
head(sleep)

g <- ggplot(sleep, aes(x = group, y = extra, group = factor(ID))) ## Plotting the data
g <- g + geom_line(size = 1, aes(colour = ID)) + geom_point(size = 10, pch = 21, fill = "salmon", alpha = .5)
g

g1 <- sleep$extra[1:10] ## Results
g2 <- sleep$extra[11:20]
difference <- g2 - g1
mn <- mean(difference)
s <- sd(difference)
n <- 10

mn + c(-1, 1) * qt(.975, n - 1) * s / sqrt(n)
t.test(difference)
t.test(g2, g1, paired = TRUE)
t.test(extra ~ I(relevel(group, 2)), paired = TRUE, data = sleep)

rbind(mn + c(-1, 1) * qt(.975, n - 1) * s / sqrt(n), as.vector(t.test(difference)$conf.int), as.vector(t.test(g2, g1, paired = TRUE)$conf.int), as.vector(t.test(extra ~ I(relevel(group, 2)), paired = TRUE, data = sleep)$conf.int)) ## The results


# T distribution data example: Pooled t test with same variances. Means and sd are given already.
sp <- sqrt((7 * 15.34 ^ 2 + 20 * 18.23 ^ 2) / (8 + 21 - 2)) ## Check the standarrd error for pooled group.
132.86 - 127.44 + c(-1, 1) * qt(.975, 27) * sp * (1 / 8 + 1 / 21) ^ .5 ## Check confidence interval for pooled group.


# Mistakenly treating the sleep data as grouped
n1 <- length(g1)
n2 <- length(g2)
sp <- sqrt(((n1 - 1) * sd(g1) ^ 2 + (n2 - 1) * sd(g2) ^ 2) / (n1 + n2 - 2))
md <- mean(g2) - mean(g1)
semd <- sp * sqrt(1/n1 + 1/n2)
rbind(md + c(-1, 1) * qt(.975, n1 + n2 - 2) * semd, t.test(g2, g1, paired = FALSE, var.equal = TRUE)$conf, t.test(g2, g1, paired = TRUE)$conf) ## Since the data is paired actually the last row is correct.


# ChickWeight data in R
library(datasets)
data(ChickWeight)
library(reshape2)
wideCW <- dcast(ChickWeight, Diet + Chick ~ Time, value.var = "weight")
names(wideCW)[-(1:2)] <- paste("time", names(wideCW)[-(1:2)], sep = "")
library(dplyr)
wideCW <- mutate(wideCW, gain = time21 - time0)

g <- ggplot(ChickWeight, aes(x = Time, y = weight, colour = Diet, group = Chick)) ## Plotting the raw data
g <- g + geom_line()
g <- g + stat_summary(aes(group = 1), geom = "line", fun.y = mean, size = 1, col = "black")
g <- g + facet_grid(. ~ Diet)
g

g <- ggplot(wideCW, aes(x = factor(Diet), y = gain, fill = factor(Diet))) ## Weight gain by diet
g <- g + geom_violin(col = "black", size = 2)
g

wideCW14 <- subset(wideCW, Diet %in% c(1, 4)) ## Let's do a t interval. Omitting diet 2 and 3 to do t test between 1 and 4.
rbind(t.test(gain ~ Diet, paired = FALSE, var.equal = TRUE, data = wideCW14)$conf, t.test(gain ~ Diet, paired = FALSE, var.equal = FALSE, data = wideCW14)$conf) ## var.equal = FALSE should be the correct coding since variance between diets are different.


# Hypothesis: T test in R
library(UsingR)
data(father.son)
t.test(father.son$sheight - father.son$fheight)
t.test(father.son$sheight - father.son$fheight, alternative = "greater")


# Hypothesis: with chickWeight data. Recall that we reformatted this data
library(datasets)
data(ChickWeight)
library(reshape2)
wideCW <- dcast(ChickWeight, Diet + Chick ~ Time, value.var = "weight")
names(wideCW)[-(1:2)] <- paste("time", names(wideCW)[-(1:2)], sep = "")
library(dplyr)
wideCW <- mutate(wideCW, gain = time21 - time0)

wideCW14 <- subset(wideCW, Diet %in% c(1, 4)) ## Unequal variance T test comparing diets 1 and 4.
t.test(gain ~ Diet, paired = FALSE, var.equal = TRUE, data = wideCW14)
t.test(gain ~ Diet, paired = FALSE, var.equal = FALSE, data = wideCW14)


# P-values.
pt(2.5, 15, lower.tail = FALSE)

choose(8, 7) * .5 ^ 8 + choose(8, 8) * .5 ^ 8
pbinom(6, size = 8, prob = .5, lower.tail = FALSE)

ppois(9, 5, lower.tail = FALSE)


# Assignment #3:
Load.Install(c("swirl", "crayon"))
install_from_swirl("Statistical Inference")
swirl()


################################## Week #4 #####################################
# Calculating power for Gaussian data
alpha <- 0.05
mu0 <- 30
mua <- 32
sigma <- 4
n <- 16
z <- qnorm(1 - alpha)
## Note that in the below coding we are using mu0 value all the time since all the tests are conducting under the null. However, changing the lower.tail argument should give your the desired type I , II errors or power.
pnorm(mu0 + z * sigma / sqrt(n), mean = mu0, sd = sigma / sqrt(n), lower.tail = TRUE) ## Accepting the null when it is TRUE.
pnorm(mu0 + z * sigma / sqrt(n), mean = mu0, sd = sigma / sqrt(n), lower.tail = FALSE) ## Rejecting the null when it is TRUE. This is type I error. Here we used lower.tail=FALSE to take the complement of the previous code.
pnorm(mu0 + z * sigma / sqrt(n), mean = mua, sd = sigma / sqrt(n), lower.tail = TRUE) ## Accepting the null wehn it is FALSE. Here note that true mean is mua but mu0 is used. This is type II error
pnorm(mu0 + z * sigma / sqrt(n), mean = mua, sd = sigma / sqrt(n), lower.tail = FALSE) ## Rejecting the null wehn it is FALSE. This is power. This is there is 64% probability of detecting a mean as large as 32 or more if we conduct this experiment.


# Plotting the power curve
library(ggplot2)
nseq <- c(8, 16, 32, 64, 128) ## Different sample sizes.
mua <- seq(30, 35, by = 0.1) ## Different alternative hypothesis means.
power <- sapply(nseq, function(n) pnorm(mu0 + z * sigma / sqrt(n), mean = mua, sd = sigma / sqrt(n), lower.tail = FALSE)) ## Give the power for different sample size and alternative hypothesis mean combinations.
colnames(power) <- paste("n", nseq, sep = "")
d <- data.frame(mua, power)
library(reshape2)
d2 <- melt(d, id.vars = "mua")
names(d2) <- c("mua", "n", "power")
g <- ggplot(d2, aes(x = mua, y = power, col = n)) + geom_line(size = 1)
g


# Graphical Depiction of Power
library(manipulate)
mu0 <-  30
myplot <- function(sigma, mua, n, alpha){
    g <- ggplot(data.frame(mu = c(27, 36)), aes(x = mu))
    g <- g + stat_function(fun = dnorm, geom = "line", args = list(mean = mu0, sd = sigma / sqrt(n)), size = 2, col = "red")
    g <- g + stat_function(fun = dnorm, geom = "line", args = list(mean = mua, sd = sigma / sqrt(n)), size = 2, col = "blue")
    xitc <- mu0 + qnorm(1 - alpha) * sigma / sqrt(n)
    g <- g + geom_vline(xintercept = xitc, size = 3)
    g
}

manipulate(
    myplot(sigma, mua, n, alpha),
    sigma = slider(1, 10, step = 1, initial = 4),
    mua = slider(30, 35, step = 1, initial = 32),
    n = slider(1, 50, step = 1, initial = 16),
    alpha = slider(0.01, 0.1, step = 0.01, initial = 0.05)
)

# T-test power
power.t.test(n = 16, delta = 2 / 4, sd = 1, type = "one.sample", alternative = "one.sided")$power ## Delta is the difference between means. Remember the effect size hiwhc is the difference between means over standard deviation. power.t.test depends on it.
power.t.test(n = 16, delta = 2, sd = 4, type = "one.sample", alternative = "one.sided")$power
power.t.test(n = 16, delta = 100, sd = 200, type = "one.sample", alternative = "one.sided")$power

power.t.test(power = 0.6040329, n = 16, sd = 200, type = "one.sample", alternative = "one.sided")$delta

power.t.test(power = .8, delta = 2 / 4, sd = 1, type = "one.sample", alternative = "one.sided")$n ## Here we specified the desired power and trying to find the necessar sample size.
power.t.test(power = .8, delta = 2, sd = 4, type = "one.sample", alternative = "one.sided")$n
power.t.test(power = .8, delta = 100, sd = 200, type = "one.sample", alternative = "one.sided")$n


# Multiple Testing:
# Case study I: no true positives
set.seed(1010093)
pValues <- rep(NA,1000)
for (i in 1:1000) {
    y <- rnorm(20)
    x <- rnorm(20)
    pValues[i] <- summary(lm(y ~ x))$coeff[2,4]
}
sum(pValues < 0.05) ## Controls false positive rate. Eventhough there is no reletion between y and x we found 51 times that null rejected which says there is a relation.
sum(p.adjust(pValues, method = "bonferroni") < 0.05) ## Controls FWER (family wise error rate) Bonferroni correction.
sum(p.adjust(pValues, method = "BH") < 0.05) ## Controls FDR (false discovery rate) Benjamini–Hochberg correctio.

# Case study II: 50% true positives
set.seed(1010093)
pValues <- rep(NA,1000)
for (i in 1:1000) {
    x <- rnorm(20)
    # First 500 beta=0 (there is no relationship), last 500 beta=2 (there is a relationship).
    if (i <= 500) {
        y <- rnorm(20)
    }
    else {
        y <- rnorm(20, mean = 2*x)
    }
    pValues[i] <- summary(lm(y ~ x))$coeff[2,4]
}
trueStatus <- rep(c("zero","not zero"), each = 500)
table(pValues < 0.05, trueStatus) ## 24 is the number of type I errors.
table(p.adjust(pValues, method = "bonferroni") < 0.05,trueStatus) ## Controls FWER
table(p.adjust(pValues, method = "BH") < 0.05,trueStatus) ## Controls FDR


par(mfrow = c(1,2))
plot(pValues, p.adjust(pValues, method = "bonferroni"), pch = 19)
plot(pValues, p.adjust(pValues, method = "BH"), pch = 19)


# Bootstraping
# Sample of 50 die rolls
library(ggplot2)
library(gridExtra)
nosim <- 1000
cfunc <- function(x, n) mean(x)
g1 <- ggplot(data.frame(y = rep(1/6, 6), x = 1:6), aes(y = y, x = x))
g1 <- g1 + geom_bar(stat = "identity", fill = "lightblue", colour = "black")

dat <- data.frame(x = apply(matrix(sample(1:6, nosim * 50, replace = TRUE), nosim), 1, mean))
g2 <- ggplot(dat, aes(x = x)) + geom_histogram(binwidth = .2, colour = "black", fill = "salmon", aes(y = ..density..))
grid.arrange(g1, g2, ncol = 2)


# What if we only had one sample?
n <- 50
B <- 1000
x <- sample(1:6, n, replace = TRUE)
resamples <- matrix(sample(x, n * B, replace = TRUE), B, n) ## bootstrap resamples
resampledMeans <- apply(resamples, 1, mean)
g1 <- ggplot(as.data.frame(prop.table(table(x))), aes(x = x, y = Freq)) + geom_bar(colour = "black", fill = "lightblue", stat = "identity")
g2 <- ggplot(data.frame(x = resampledMeans), aes(x = x)) + geom_histogram(binwidth = .2, colour = "black", fill = "salmon", aes(y = ..density..))
grid.arrange(g1, g2, ncol = 2)


# Consider a data set
library(UsingR)
data(father.son)
x <- father.son$sheight
n <- length(x)
B <- 10000 ## Number of bootstraps.
resamples <- matrix(sample(x, n * B, replace = TRUE), B, n)
resampledMedians <- apply(resamples, 1, median)

## A plot of the histrogram of the resamples
g <- ggplot(data.frame(x = resampledMedians), aes(x = x))
g <- g + geom_density(size = 1, fill = "red")
g <- g + geom_vline(xintercept = median(x), size = 1)
g


# Bootstrap: Example code
library(UsingR)
data(father.son)
x <- father.son$sheight
n <- length(x)
B <- 10000
resamples <- matrix(sample(x, n * B, replace = TRUE), B, n)
medians <- apply(resamples, 1, median)
sd(medians)
quantile(medians, c(.025, .975)) ## quantile function can be used for any empirical distribution. Note that these confidence interval is real pure. So the recomended approach is using bias corrected and accelerated interval (BSA) interval which is available in bootstrap package in R.

g = ggplot(data.frame(medians = medians), aes(x = medians)) ## Histogram of bootstrap resamples.
g = g + geom_histogram(color = "black", fill = "lightblue", binwidth = 0.05)
g


# Assignment #4:
Load.Install(c("swirl", "crayon"))
install_from_swirl("Statistical Inference")
swirl()


################################ Assignment ####################################


























